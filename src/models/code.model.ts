import { CodeGenerator } from "@libs/codeGenrator";
import { Schema, Document, Model, model, Types } from "mongoose";
export enum CodeTypes {
    Activation = `Activation`,
    PasswordReset = `PasswordReset`
}

export interface IBaseCode {
    code: string;
    userId: Types.ObjectId;
    expire: Date;
    type: string;
}

export interface ICode extends IBaseCode, Document {
    createCode(type: CodeTypes, user: Types.ObjectId):Promise<string | null>;
}

export const CodeSchema = new Schema(
  {
      code: {
          type: String,
          required: true,
      },
      userId: {
          type: Types.ObjectId,
          required: true
      },
      expire: {
          type: Date,
          required: true
      },
      type: {
          type: String,
          required: true,
          enum: {
            values: ['Activation', 'PasswordReset'],
            message: '{VALUE} is not supported'
          }
      }
  },
  {
    minimize: false,
    timestamps: true,
    collection: "Codes",
    toJSON: { getters: true, virtuals: true },
    toObject: { getters: true, virtuals: true },
  }
);

CodeSchema.statics.createCode = async function(type: CodeTypes, user: Types.ObjectId):Promise<string | null> {
    let code = CodeGenerator.genereate(12);
    const date = new Date();
    date.setDate(date.getDate() + 1);
    let newCode = new CodeModel({
        code: code,
        userId: user,
        expire: date,
        type: type
    });

    try {
        await newCode.save();
        return code;
    } catch (error) {
        console.log(`Error to generate activation Code for user ${error} `);
    }
    return null;
}

export interface Code extends Model<ICode> {
    createCode(type: CodeTypes, user: Types.ObjectId):Promise<string | null>;
}

export const CodeModel = model<ICode, Code>("Codes", CodeSchema);
