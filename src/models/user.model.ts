import { Schema, Document, Model, model } from "mongoose";
import * as bcrypt from "bcryptjs";

export interface IBaseForLogin {
  firstName: string;
  lastName: string;
  userName: string;
  email: string;
  status?: number;
  roles?: Array<string>;
  token?: string;
  image: string;
}
export interface IBaseUser extends IBaseForLogin {
  password: string;
  tokens?: Array<string>;
}

export interface IUser extends IBaseUser, Document {
  passwordCheck(password: string): boolean;
  loginUser(token: string): Promise<IBaseForLogin>;
  hashPassword(password: string): string;
  convertToBasUser(userData: IUser): IBaseForLogin;
  createNewUser(userData: IBaseUser): Promise<IUser | null>
}

export const UserSchema = new Schema(
  {
    firstName: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },
    userName: {
      type: String,
      required: false,
      default: "",
    },
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
    },
    status: {
      type: Number,
      required: false,
      default: 0,
    },
    image: {
      type: String,
      required: false,
      default: ``
    },
    roles: {
      type: [String],
      required: false,
      default: [],
    },
    tokens: {
      type: [String],
      required: false,
      default: [],
    },
  },
  {
    minimize: false,
    timestamps: true,
    collection: "Users",
    toJSON: { getters: true, virtuals: true },
    toObject: { getters: true, virtuals: true },
  }
);

UserSchema.methods.passwordCheck = function (
  password: string
): boolean {
  return bcrypt.compareSync(password, this.password);
};

UserSchema.methods.loginUser = async function (
  token: string
): Promise<IBaseForLogin> {
  this.tokens.push(token);
  await this.save();
  return {
    firstName: this.firstName,
    lastName: this.lastName,
    userName: this.userName ? this.userName : "",
    email: this.email,
    status: this.status,
    roles: this.roles ? this.roles : [],
    token: token,
    image: this.image
  };
};
UserSchema.statics.convertToBasUser = function (
  userData: IUser
): IBaseForLogin {
  return {
    firstName: userData.firstName,
    lastName: userData.lastName,
    userName: userData.userName,
    email: userData.email,
    roles: userData.roles,
    status: userData.status,
    image: userData.image
  };
};

UserSchema.statics.hashPassword = function (password: string): string {
  return bcrypt.hashSync(password);
};

UserSchema.statics.createNewUser = async function (
  userData: IBaseUser
): Promise<IUser | null> {
  userData.password = UserModel.hashPassword(userData.password);
  userData.status = 1;
  userData.roles = [`login`];
  let newUser = new UserModel(userData);
  try {
    return await newUser.save();
  } catch (error) {
    console.log(`Create user error: ${error}`);
  }
  return null;
};

export interface User extends Model<IUser> {
  createNewUser(userData: IBaseUser): Promise<IUser | null>;
  passwordCheck(password: string): boolean;
  loginUser(token: string): Promise<IBaseForLogin>;
  hashPassword(password: string): string;
  convertToBasUser(userData: IUser): IBaseForLogin;
}

export const UserModel = model<IUser, User>("Users", UserSchema);
