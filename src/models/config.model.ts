export interface IConfig {
    backendUrl: string;
    jwtSecret: string;
    serverPort: number;
    frontendURL: string;
    mongoDB: string;
    mail_host?: string;
    mail_port?: number;
    mail_user?: string;
    mail_password?: string;
    mail_from?: string;
}