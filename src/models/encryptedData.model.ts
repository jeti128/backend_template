import { Encryper } from "@libs/encryper.lib";
import { Schema, Document, Model, model, Types } from "mongoose";

export interface IBaseEncryptedData {
    encryptedData: string;
    userId: Types.ObjectId;
}

export interface IEncryptedData extends IBaseEncryptedData, Document {
    createEncryptedData():Promise<string | null>;
}

export const EncryptedDataSchema = new Schema(
  {
      encryptedData: {
          type: String,
          required: true,
      },
      userId: {
          type: Types.ObjectId,
          required: true
      }
  },
  {
    minimize: false,
    timestamps: true,
    collection: "EncryptedDatas",
    toJSON: { getters: true, virtuals: true },
    toObject: { getters: true, virtuals: true },
  }
);

EncryptedDataSchema.statics.createEncryptedData = async function(data: string, userId: Types.ObjectId): Promise<IEncryptedData | null> {
    let newEncryptedData = new EncryptedDataModel({
        encryptedData: data,
        userId: userId
    });

    try {
        return await newEncryptedData.save();
    } catch (error) {
        console.log(`Error to store encryped data: ${error}`);
    }

    return null;
}

export interface EncryptedData extends Model<IEncryptedData> {
}

export const EncryptedDataModel = model<IEncryptedData, EncryptedData>("EncryptedDatas", EncryptedDataSchema);