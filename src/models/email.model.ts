export interface IMailConfig {
    host: string;
    port: number;
    user: string;
    password: string;
    from: string;
  };
  
  export interface IMail {
    to: string;
    subject: string;
    message: string;
    htmlMessage?: string;
  };

  export interface IBaseMail{
    send(): Promise<Boolean> ;
  }