import { Request, Response } from "express";
import {UserModel, IUserModel} from "@models/UserModel";

export class UserAdminController {
    public static async get(req: Request, res: Response): Promise<void> {
        let usersData = await UserModel.find({}).exec();

        res.send(usersData);
    }
}
