import * as jwt from "jsonwebtoken";
import { Config } from "@config/config";
import { IUser, UserModel } from "@models/user.model";
import { Router, Request, Response } from "express";
import { IBaseForLogin } from "@models/user.model";
import { Validator } from "@libs/validator";
import { Types } from "mongoose";
import { CodeModel, CodeTypes } from "@models/code.model";
import { ActtivationMail } from "@libs/mail/activation.mail";
import { UploadedFile } from "express-fileupload";
import { UserImageFile } from "@libs/file/userImage.file";

let config = Config.getConfig();

export class UserController {
  public static async loginFunction(
    email: string,
    password: string
  ): Promise<IUser | null> {
    let userData: IUser | null = await UserModel.findOne({ email }).exec();

    if (userData?.passwordCheck(password)) {
      return userData;
    }

    return null;
  }

  public static async doLogin(
    email: string,
    password: string
  ): Promise<IBaseForLogin | null> {
    let userData: IUser | null = await UserController.loginFunction(
      email,
      password
    );
    if (userData) {
      if (userData.status && userData.status > 1) {
        const token = jwt.sign(
          { userId: userData.id, email: userData.email },
          config.jwtSecret,
          { expiresIn: "10h" }
        );
        return userData.loginUser(token);
      }
    }

    return null;
  }

  public static async Singup(request: Request, response: Response) {
    let newUserData: IUser = request.body;

    if (!newUserData.email || !newUserData.password || !newUserData.userName) {
      response.status(400).send({
        error: `email or username or password field not found`,
      });
      return;
    }

    if (newUserData.email.match(Validator.email) === null) {
      response.status(400).send({
        error: `email is not vaild`,
      });
      return;
    }

    if (newUserData.password.match(Validator.password) === null) {
      response.status(400).send({
        error: `password is not vaild (need one upper and one lower and number and min legth 8)`,
      });
      return;
    }
    let user = await UserModel.createNewUser(newUserData);
    console.log(user);

    if (user) {
      let code = await CodeModel.createCode(CodeTypes.Activation, user?._id);
      if (code) {
        let email = new ActtivationMail(user.email, {
          code: code,
          user: user,
        });
        await email.send();
        response.send({ status: true });
        return;
      }
    }
    response.status(500).send({ status: false, error: "User create error" });
  }

  public static async login(request: Request, response: Response) {
    let { email, password } = request.body;

    if (!email || !password) {
      response.status(400).send(`username or password field not found`);
      return;
    }
    let userData: IBaseForLogin | null = await UserController.doLogin(
      email,
      password
    );

    if (!userData) {
      response.status(400).send(`Login error`);
      return;
    }

    response.send(userData);
  }

  public static async getUserData(request: Request, response: Response) {
    let userData: IUser | null = null;
    response.send(
      <IBaseForLogin>UserModel.convertToBasUser(request.user as IUser)
    );
  }

  public static async changePassword(request: Request, response: Response) {
    const oldPassword = request.body.oldPassword;
    const newPassword = request.body.newPassword;

    if (!oldPassword || !newPassword) {
      response.status(400).send(`Missing old or new password field`);
      return;
    }

    if (!request.user) {
      response.status(400).send(`jwt error`);
      return;
    }
    let userData: IUser | null = await UserModel.findOne({
      _id: new Types.ObjectId((request.user as IUser)._id),
    }).exec();

    if (!userData) {
      response.status(400).send(`User data not found`);
      return;
    }

    if (!(await userData.passwordCheck(oldPassword))) {
      response.status(400).send(`Old password is bad`);
      return;
    }

    if (newPassword.match(Validator.password) === null) {
      response
        .status(400)
        .send(
          `password is not vaild (need one upper and one lower and number and min legth 8)`
        );
      return;
    }

    userData.password = UserModel.hashPassword(newPassword);

    try {
      await userData.save();
      response.send({ status: true });
    } catch (error) {
      console.log(`User password change error on save: ${error}`);
      response.status(500).send(`Server internal error`);
    }
  }

  public static async edit(request: Request, response: Response) {
    if (!request.body.data) {
      response.status(400).send(`Input data not found`);
      return;
    }
    let editUserData: IBaseForLogin = JSON.parse(request.body.data);

    let userData: IUser | null = await UserModel.findOne({
      _id: new Types.ObjectId((request.user as IUser)._id),
    }).exec();

    if (!userData) {
      response.status(400).send(`User data not found`);
      return;
    }

    if (editUserData.firstName) {
      userData.firstName = editUserData.firstName;
    }
    if (editUserData.lastName) {
      userData.lastName = editUserData.lastName;
    }
    if (editUserData.userName) {
      userData.userName = editUserData.userName;
    }

    if (request.files) {
      if (request.files.image) {
        let file: UploadedFile = request.files.image as UploadedFile;
        let UIService = new UserImageFile();
        const imageLink = UIService.saveUserImage(file, userData._id);
        if (imageLink) {
          userData.image = imageLink;
        }
      }
    }

    try {
      await userData.save();
      response.send(UserModel.convertToBasUser(userData));
    } catch (error) {
      console.log(`Edit user data: ${error}`);
      response.status(500).send(`ServerInternal error`);
    }
  }

  public static async activatio(request: Request, response: Response) {
    console.log(request.params);
    if (!request.params.id) {
      response.status(500).send(`not found Activation code`);
      return;
    }
    let code = await CodeModel.findOne({ code: request.params.id }).exec();

    if (!code) {
      response.status(400).send(`Not found this activation code`);
      return;
    }

    if (new Date() > code.expire) {
      response.status(400).send(`This activation code is expired`);
      return;
    }

    let user = await UserModel.findOne({ _id: code.userId }).exec();

    if (!user) {
      response.status(400).send(`Not found user for this activation code`);
      return;
    }

    if (user.status !== 1) {
      response.status(400).send(`User status is not: wait activation`);
      return;
    }

    user.status = 2;

    try {
      await user.save();
      response.redirect(`${config.frontendURL}/singup-activation`);
    } catch (error) {
      console.log(`User activation save user error: ${error}`);
      response.status(500).send(`Internal error`);
      return;
    }
  }
}
