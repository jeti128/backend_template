import { Router, Request, Response } from "express";
import { checkJwt } from "@middleware/checkJwt";
import { EncryptContoller } from "@controllers/encrypt.controller";

export const encryptRouter = Router();

encryptRouter.use(`/*`, checkJwt);

encryptRouter.get(`getEncrypts`, EncryptContoller.getEncrypts);