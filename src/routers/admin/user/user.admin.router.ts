import { Router, Request, Response } from "express";
import { checkRouls } from "@middleware/checkRoule";
import { UserAdminController } from "@controllers/admin/user/user.admin.controller.ts";

export const userAdminRouter = Router();

userAdminRouter.use("/*", checkRouls(["AdminUserEdit"]))

userAdminRouter.get("/get", UserAdminController.get);

userAdminRouter.get("/", (request: Request, response: Response) => {
  response.send("/admin/user");
});

