import { Router, Request, Response } from "express";
import { checkJwt } from "@middleware/checkJwt";
import { checkRouls } from "@middleware/checkRouls";

export const adminRouter = Router();

adminRouter.use(`/*`, checkJwt);
adminRouter.use("/*", checkRouls(["admin"]))


adminRouter.get("/", (request: Request, response: Response) => {
  response.send("/admin");
});


