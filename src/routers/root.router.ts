import express, { Router, Request, Response } from "express";
import { userRouter } from "./user/user.router";
import { userAuthedRouter } from "./user/user.authed.router";
import { adminRouter } from "./admin/admin.router";

import path from "path";

export const rootRouter = Router();

rootRouter.use(`/user`, userRouter)
rootRouter.use(`/userAuthed`, userAuthedRouter)
rootRouter.use(`/public`, express.static(path.join(__dirname, '../../data')))
rootRouter.use(`/admin`, adminRouter);

rootRouter.get("/", (request: Request, response: Response) => {
  response.send("Hello word!");
});

