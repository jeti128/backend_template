export class CodeGenerator {
    public static Source = "abcdefghiklmnopqrstvxyzABCDEFGHIKLMNOPQRSTVXYZ1234567890";
    public static getRandomChar(): string {
        return CodeGenerator.Source[Math.floor(Math.random() * CodeGenerator.Source.length)];
    }

    public static genereate(length: number): string {
        let code = "";
        let a = 0;
        while (a < length) {
            code += CodeGenerator.getRandomChar();
            a ++;
        }

        return code;
    }
}