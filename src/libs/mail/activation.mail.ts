import { IUser } from "@models/user.model";
import { Email } from "./emial";

interface IActvationMail {
  code: string;
  user: IUser;
}

export class ActtivationMail extends Email<IActvationMail> {
  public subject: string = `Regisztracio`;
  public mailHTML(): string {
    return `<h1>Üdvözöllek az oldalamon ${this.data.user.firstName} ${this.data.user.lastName}!</h1>
        <br/>
        <br/>
        Kérlek ativálad a hozzáférésedet az oldalhoz ezzel a linkel:
        <a href="${this.config.backendUrl}:${this.config.serverPort}/user/activation/${this.data.code}">Activacio</a>
        `;
  }
}
