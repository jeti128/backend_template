import { IConfig } from "@models/config.model";
import { MailClient } from "@libs/email.client";
import { IBaseMail } from "@models/email.model";
import { Config } from "@config/config";

export class Email<T> implements IBaseMail {
  public config: Config;
  public mailClient: MailClient;
  public subject: string = ``;
  public data: T;
  public to: string;

  constructor(to: string, data: T) {
    this.to = to;
    this.data = data;
    this.config = Config.getConfig();
    this.mailClient = new MailClient(this.config.getMailConfig());
  }

  public mailHTML(): string {
    return ``;
  }
  public mailRaw(): string {
    return ``;
  }

  public async send(): Promise<Boolean> {
    return await this.mailClient.sendMail({
      to: this.to,
      subject: this.subject,
      htmlMessage: this.mailHTML(),
      message: this.mailRaw(),
    });
  }
}
