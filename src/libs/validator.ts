export class Validator {
    /*
     * regex collections
     */
    public static email = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,10}$/;
    public static password = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,32}$/;

}