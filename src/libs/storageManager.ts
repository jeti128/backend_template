import { UploadedFile } from "express-fileupload";
import { existsSync, mkdirSync } from "fs";
export class StorageManager {
  public baseLink: string = process.cwd() + `/data/`;
  constructor() {}

  protected saveFile(file: UploadedFile | null, to: string): boolean {
    if (!file) {
      console.log(`File save error: input file not exists`);
      return false;
    }
    const path = `${this.baseLink}${to}`;

    file.mv(path, (error) => {
      if (error) {
        console.log(`User edit profile image save error: ${error}`);
        return false;
      }
    });
    return true;
  }

  protected folderExists(link: string): boolean {
      link = `${this.baseLink}${link}`;
    if (existsSync(link)) {
      return true;
    } else {
      return false;
    }
  }

  protected createFolder(link: string): boolean {
    link = `${this.baseLink}${link}`;

    try {
        mkdirSync(link, 0o744);
        return true;
    } catch (error) {
        console.log(`Error to create folder: ${error}`);
        return false;
    }
  }
}
