import { createTransport, Transporter } from "nodemailer";
import { IMail, IMailConfig } from "@models/email.model";

export class MailClient {
  private transporter: Transporter | null = null;
  private from: string = ``;

  constructor(config: IMailConfig) {
    this.transporter = createTransport({
      host: config.host,
      port: config.port,
      secure: false, // true for 465, false for other ports
      auth: {
        user: config.user, // generated ethereal user
        pass: config.password, // generated ethereal password
      },
    });

    this.from = config.from;
  }

  public async sendMail(mailData: IMail): Promise<boolean> {
    if (!this.transporter) {
      return false;
    }
    let info = await this.transporter.sendMail({
      from: this.from,
      to: mailData.to,
      subject: mailData.subject,
      text: mailData.message ? mailData.message : ``,
      html: mailData.htmlMessage ? mailData.htmlMessage : ``,
    });

    return true;
  }
}