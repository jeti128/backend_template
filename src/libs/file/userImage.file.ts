import { StorageManager } from "@libs/storageManager";
import { UploadedFile } from "express-fileupload";
import { Types } from "mongoose";

export class UserImageFile extends StorageManager {
  public saveUserImage(file: UploadedFile, userID: Types.ObjectId): string | null {
      let imageFolder = `users/${userID}`;
    let path = `${imageFolder}/${file.name}`;
    if (!this.folderExists(imageFolder)) {
        this.createFolder(imageFolder);
    }
    this.saveFile(file, path);
    return path;
  }
}
