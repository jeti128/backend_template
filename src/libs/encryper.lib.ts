import {enc,  SHA256, AES } from "crypto-ts";

export class Encryper {
    public static encriptData(key: string, data: string): string {
        return AES.encrypt(data, key).toString();
    }

    public static decrypted(key: string, data: string): string {
        return AES.decrypt(data, key).toString(enc.Utf8);
    }
}