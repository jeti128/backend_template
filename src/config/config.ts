import { IConfig } from "@models/config.model";
import { IMailConfig } from "@models/email.model";

let config: Config | null = null;

export class Config implements IConfig {
  //public backendUrl = `http://jeti128.ddns.net`;
  //public frontendURL = `http://jeti128.ddns.net/`;
  public backendUrl = `http://192.168.88.40:8080`;
  public frontendURL = `http://192.168.88.40:4200/`;
  public jwtSecret = "Aztamekkoraegykamu";
  public serverPort = 8080;
  public mongoDB = `mongodb+srv://test:Fullacc12@cluster0.gxrqx.mongodb.net/test`;
  public mail_host = `smtp.digikabel.hu`;
  public mail_port = 25;
  public mail_user = `jeti128@digikabel.hu`;
  public mail_password = `b8HGx7TL`;
  public mail_from = `jeti128@digikabel.hu, jeti128@digikabel.hu`;
  /*
  constructor(config?: IConfig) {
    if (config) {
      this.jwtSecret = config.jwtSecret
        ? config.jwtSecret
        : `aztamekkoraegyfassag`;
        this.serverPort = config.serverPort
        ? config.serverPort
        : 8080;
        this.mongoDB = config.mongoDB
        ? config.mongoDB
        : ``;
    }
  }*/

  public static getConfig(): Config {
    if (!config) {
      config = new Config();
    }

    return config;
  }

  public getMailConfig(): IMailConfig {
    return {
      host: this.mail_host,
      port: this.mail_port,
      user: this.mail_user,
      password: this.mail_password,
      from: this.mail_from
    }
  }
}
