import { Encryper } from "@libs/encryper.lib";

let key = "Akármi ami akarsz";
let message: string = "Message: this is message for test encrypt!";

let encryptedData: string = "";

describe(`test Ecrypter: `, () => {
    it(`Encrypt data: `, () => {
        encryptedData = Encryper.encriptData(key,message);
    });
    it(`Decrypte data:`, () => {
        let originalMessage = Encryper.decrypted(key, encryptedData);
        expect(originalMessage).toBe(message);
    });
});