import { Validator } from "@libs/validator";


describe(`Validator email`, () => {
    test(`Email validation`, () => {
        let email =`jeti128@gmail.com`;
        expect(email.match(Validator.email)).not.toBe(null);
    });
    test(`Email validation fail (not found @)`, () => {
        let email =`jeti128gmail.com`;
        expect(email.match(Validator.email)).toBe(null);
    });
    test(`Email validation fail (not found .)`, () => {
        let email =`jeti128@gmailcom`;
        expect(email.match(Validator.email)).toBe(null);
    });
    test(`Email validation fail (not found name)`, () => {
        let email =`@gmail.com`;
        expect(email.match(Validator.email)).toBe(null);
    });
    test(`Email validation fail (not found domain)`, () => {
        let email =`jeti128@.com`;
        expect(email.match(Validator.email)).toBe(null);
    });
});

describe(`Validator password`, () => {
    test(`Password validate`, () => {
        let passowrd = `Abcde123`;
        expect(passowrd.match(Validator.password)).not.toBe(null)
    });
    test(`Password validate fail (too low)`, () => {
        let passowrd = `Abcde12`;
        expect(passowrd.match(Validator.password)).toBe(null)
    });
    test(`Password validate fail (no number)`, () => {
        let passowrd = `Abcdefgh`;
        expect(passowrd.match(Validator.password)).toBe(null)
    });
    test(`Password validate fail (no upper char)`, () => {
        let passowrd = `Abcde12`;
        expect(passowrd.match(Validator.password)).toBe(null)
    });
    test(`Password validate fail (no lower char)`, () => {
        let passowrd = `ABCDEFG12`;
        expect(passowrd.match(Validator.password)).toBe(null)
    });
});