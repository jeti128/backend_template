import { Config } from "@config/config";
import { MailClient } from "@libs/email.client"
import { IMail } from "@models/email.model";

let config = Config.getConfig();

describe(`Test Emial client`, () => {
    it(`Email send`, async () => {
        let emailClient = new MailClient(config.getMailConfig());

        let data: IMail = {
            to: `jeti128@gmail.com`,
            subject: `singup`,
            message: `singup`,
            htmlMessage: `<h1>singup</h1>`
        };

        expect(await emailClient.sendMail(data)).toBe(true);
    });
})