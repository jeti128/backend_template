import { TestConfig } from "@config/configtest";
import { UserModel } from "@models/user.model";
import { httpService, RequestType } from "../test.client";
const baseURL = TestConfig.backendLink;

let token: string = ``;

export function userTest() {
  describe(`User test`, () => {
    test(`Singup test`, async () => {
      let data = await httpService(RequestType.post, `${baseURL}/user/singup`, {
        firstName: `Janovicz`,
        lastName: `Gabor`,
        password: `Abcde1234`,
        userName: `Jeti128`,
        email: `jeti128@gmail.com`,
      });
      expect(data.data).toEqual({ status: true });
      let userData = await UserModel.findOne({ email: `jeti128@gmail.com`}).exec();
      if (userData) {
        userData.status = 2;
        await userData.save();
      }
    });

    test(`Login test`, async () => {
      let data = (
        await httpService(RequestType.post, `${baseURL}/user/login`, {
          email: `jeti128@gmail.com`,
          password: `Abcde1234`,
        })
      ).data;

      token = data.token;

      delete data.token;
      expect(data).toEqual({
        firstName: `Janovicz`,
        lastName: `Gabor`,
        userName: `Jeti128`,
        email: `jeti128@gmail.com`,
        roles: [`login`],
        status: 2,
      });
    });

    test(`Get user data (token test)`, async () => {
      const config = {
        headers: { Authorization: `Bearer ${token}` },
      };
      let data = await (
        await httpService(
          RequestType.get,
          `${baseURL}/userAuthed/getUserData`,
          null,
          config
        )
      ).data;
      expect(data).toEqual({
        firstName: `Janovicz`,
        lastName: `Gabor`,
        userName: `Jeti128`,
        email: `jeti128@gmail.com`,
        roles: [`login`],
        status: 2,
      });
    });

    test(`change password fail (no input fields)`, async () => {
      const config = {
        headers: { Authorization: `Bearer ${token}` },
      };

      let data = await await httpService(
        RequestType.post,
        `${baseURL}/userAuthed/changePassword`,
        {
          oldPassword: ``,
          newPassword: ``,
        },
        config
      );
      expect(data.status).toEqual(400);
      expect(data.data).toEqual(`Missing old or new password field`);
    });

    test(`change password fail (bad new password)`, async () => {
      const config = {
        headers: { Authorization: `Bearer ${token}` },
      };

      let data = await await httpService(
        RequestType.post,
        `${baseURL}/userAuthed/changePassword`,
        {
          oldPassword: `Abcde1234`,
          newPassword: `aBcde`,
        },
        config
      );
      expect(data.status).toEqual(400);
      expect(data.data).toEqual(
        `password is not vaild (need one upper and one lower and number and min legth 8)`
      );
    });

    test(`change password fail (bad old password)`, async () => {
      const config = {
        headers: { Authorization: `Bearer ${token}` },
      };

      let data = await await httpService(
        RequestType.post,
        `${baseURL}/userAuthed/changePassword`,
        {
          oldPassword: `Abcde12345`,
          newPassword: `aBcde1234`,
        },
        config
      );
      expect(data.status).toEqual(400);
      expect(data.data).toEqual(`Old password is bad`);
    });

    test(`change password`, async () => {
      const config = {
        headers: { Authorization: `Bearer ${token}` },
      };

      let data = await await httpService(
        RequestType.post,
        `${baseURL}/userAuthed/changePassword`,
        {
          oldPassword: `Abcde1234`,
          newPassword: `aBcde1234`,
        },
        config
      );
      expect(data.status).toEqual(200);
      expect(data.data).toEqual({ status: true });
    });

    test(`Login test (after change password)`, async () => {
      let data = (
        await httpService(RequestType.post, `${baseURL}/user/login`, {
          email: `jeti128@gmail.com`,
          password: `aBcde1234`,
        })
      ).data;

      token = data.token;
      delete data.token;
      expect(data).toEqual({
        firstName: `Janovicz`,
        lastName: `Gabor`,
        userName: `Jeti128`,
        email: `jeti128@gmail.com`,
        roles: [`login`],
        status: 2,
      });
    });

    test(`Edit user data`, async () => {
      const config = {
        headers: { Authorization: `Bearer ${token}` },
      };

      let data = await httpService(
        RequestType.post,
        `${baseURL}/userAuthed/edit`,
        {
          firstName: `Kiss`,
          lastName: `Tibor`,
          userName: `KTib`,
        },
        config
      );
      expect(data.status).toEqual(200);
      expect(data.data.firstName).toEqual(`Kiss`);
      expect(data.data.lastName).toEqual(`Tibor`);
      expect(data.data.userName).toEqual(`KTib`);
    });
  });
}
