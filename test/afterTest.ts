import "module-alias/register";
import { Config } from "@config/config";
import { connect, connection, Mongoose } from "mongoose";

let config = Config.getConfig();
config.mongoDB += `_test`;
let d = new Date();

var datestring =
  d.getFullYear() +
  "_" +
  (d.getMonth() + 1) +
  "_" +
  d.getDate() +
  "_" +
  d.getHours() +
  "_" +
  d.getMinutes();

async function prepare(): Promise<Mongoose> {
  let conn = await connect(config.mongoDB, {});
  if (!conn) {
    console.log(`Connect error`);
  }

  return conn;
}

async function clean() {
  let conn = await prepare();
  if (!(await conn.connection.db.dropDatabase())) {
    console.log(`DB not deleted`);
  }
  connection.close();
}
if (process.argv.indexOf(`--no-clean`) === -1) {
  clean();
}
